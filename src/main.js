// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import DefaultLayout from '~/layouts/Default.vue'

import '~/assets/scss/global.scss'

import { inject } from '@vercel/analytics'

inject()

export default function (Vue, { router, head, isClient }) {
    head.htmlAttrs = { lang: 'tr' } // , amp: true
    head.bodyAttrs = { class: 'dark' }

    head.meta.push(
        {
            name: 'author',
            content: 'Yunus Emre MERT'
        },
        {
            property: 'og:site_name',
            content: 'Yunus Emre MERT'
        },
        {
            property: 'og:type',
            content: 'website'
        },
        {
            property: 'og:locale',
            content: 'tr_TR'
        },
        {
            property: 'fb:app_id',
            content: ''
        },
        {
            property: 'fb:pages',
            content: '108786901305082'
        },
        {
            name: 'twitter:card',
            content: 'summary_large_image'
        },
        {
            name: 'twitter:site',
            content: '@yunusemremert'
        },
        {
            name: 'twitter:creator',
            content: '@yunusemremert'
        },
        {
            name: 'robots',
            content: 'NOODP, NOYDIR'
        }
    )

    // Set default layout as a global component
    Vue.component('Layout', DefaultLayout)
}
