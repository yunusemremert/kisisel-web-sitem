// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

const siteName = 'Yunus Emre MERT'

module.exports = {
    siteName: siteName,
    siteUrl: 'https://yunusemremert.com.tr',
    titleTemplate: '%s - ' + siteName,
    icon: './src/assets/favicon.svg',
    plugins: [
        {
            use: '@gridsome/plugin-google-analytics',
            options: {
                id: 'UA-190731444-1'
            }
        }
    ],
    chainWebpack: (config) => {
        const svgRule = config.module.rule('svg')
        svgRule.uses.clear()
        svgRule.use('vue-svg-loader').loader('vue-svg-loader')
    }
}
